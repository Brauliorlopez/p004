/*1. Diseña una función que reciba como argumento un arreglo de valores
enteros de 20 posiciones , regrese el valor promedio de los elementos del arreglo*/

//funcion para dar valores enteros
function aleatorios() {
    return Math.floor((Math.random() * (100 - 1 + 1)) + 1);
}

//Asginacion de valores
var arreglo1 = Array(20);
for (let i = 0; i < 20; i++) {
    arreglo1[i] = aleatorios();
    
    
}


var conteo = 0;
function valorPromedio(arreglo1) {

    for (let i = 0; i < 20; i++) {
        conteo = conteo + arreglo1[i];
    }
    return conteo / 20;
}



/*2. Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, y me regrese la cantidad de valores pares que existe en el
arreglo*/

var conpares = 0;
//contar Numeros pares 
function conteoPares(arreglo1) {
    for (let i = 0; i < 20; i++) {
        if ((arreglo1[i] % 2) == 0) {
            conpares = conpares + 1;
        }
    }
    return conpares;
}



/* 3. Diseñe una función que reciba como argumento un arreglo de 20 valores
numéricos enteros, ordene los valores del arreglo de mayor a menor. */

function orden(arreglo1) {
    arreglo1.sort(function (a, b) { return b - a; });
    return arreglo1;
}


function Mostrar() {
    //Mostrar arreglo
    let parrafo = document.getElementById('arreglo');
    parrafo.innerHTML = null;
    for (let con = 0; con < 21; con++) {
        arreglo1[con] = aleatorios();
        parrafo.innerHTML = parrafo.innerHTML + arreglo1[con] + " ";
    }
    //impresión del arreglo
    console.log("El arreglo es: " + arreglo1);

    //Imprimir
    var parrafo1 = document.getElementById('funcion1');
    var mod = valorPromedio(arreglo1);
    parrafo1.innerHTML = null;
    parrafo1.innerHTML = (mod - valorPromedio(arreglo1)).toFixed(4);

    //impresión del promedio
    console.log("El promedio es: " + valorPromedio(arreglo1).toFixed(4));


    var parrafo2 = document.getElementById('funcion2');
    var mod2 = conteoPares(arreglo1);
    parrafo2.innerHTML = null;
    parrafo2.innerHTML = parrafo2.innerHTML + (mod2 - conteoPares(arreglo1));

    //Impresion de los numeros pares
    console.log("Los numeros pares son: " + conteoPares(arreglo1));

    let orden11 = orden(arreglo1);
    let parrafo3 = document.getElementById('funcion3');
    parrafo3.innerHTML = null;
    parrafo3.innerHTML = parrafo3.innerHTML + orden11 + " ";

    //Mayor menor
    console.log("El arreglo mayor menor es: " + orden(arreglo1));
}
